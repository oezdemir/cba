# Image data preparation

This document contains recommendations of how to organise your image data, e.g. when seeking consultancy at EMBL-CBA.

## Select a minimal set of representative examples

Select **two** (not more) images that ideally show a clear and biologically relevant difference.

For example the first image could be a control sample, whereas the second image has been treated in some way.

Please organise those two images into a folder structure similar to the one below:

- minimal-example-data
    - condition01
        - condition01_image01
    - condition02
        - condition02_image01
    - README.txt

As indicated please also add a short README text file where you describe in a few words what the difference is that you see between the conditions.

## Sharing your image data set

If the selected minimal data are less than a few tens of GB, please copy them onto the `\cba\exchange` network share.

Everybody has access with their normal EMBL user account.

Please be aware that **everyone at EMBL can access this data**; if you want to share data in a more private way please contact image-analysis-support@embl.de to discuss different options (e.g. using EMBL's [own-cloud](https://oc.embl.de)).

### How to access `/cba/exchange`

- Windows: Mapping `\\cba\cba\exchange`
    - Make sure you are in the EMBL intranet 
    - Use "Map network drive" within "Windows Explorer" and enter
        - \\\\cba\cba\exchange (if it does not work, try \\cba.embl.de\cba\exchange)
    - you can log in with your normal embl account:
        - embl\USERNAME
        - PASSWORD
- MacOS: Mapping `/Volumes/cba/exchange`
    - Make sure you are in the EMBL intranet 
    - Within Finder, "Go > Connect to Server" and enter
    - `cifs://USERNAME:*@cba.embl.de/cba/exchange`
    - replacing `USERNAME` by your embl username
    - In Finder, you can now "Go > Go to Folder" and enter `/Volumes/cba/exchange`
- Linux: Mapping `/g/cba/exchange`
    - Open a terminal window and install cifs (e.g. apt-get install cifs-utils)
    - mkdir -p ~/MYLOCALMOUNTPOINT
    - sudo mount -t cifs -o uid=$(id -u),gid=$(id -g),username=USERNAME //cba.embl.de/cba/exchange ~/MYLOCALMOUNTPOINT
    - replacing `USERNAME` by your embl username
    - replacing `MYLOCALMOUNTPOINT` by a folder name that you like

After you mounted `/cba/exchange` you may create a folder there with your name simply copy the data.



  
  

