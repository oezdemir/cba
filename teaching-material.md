# Bioimage analysis training resources

## Imaris

- [Official Imaris video tutorials](https://imaris.oxinst.com/learning/?businesses=bitplane&media=VideoTutorials) 

## Fiji's Trainable Weka Segmentation

- [Written documentation](https://imagej.net/plugins/tws/)
- [Live demo video](https://youtu.be/8yfBHiGufFE)

## CBA tutorial videos

- [Practical tips for inspecting big image data in Fiji (September 2021)](
https://mediasite.embl.de/Mediasite/Play/18c4db0d4e3847249bdf81b28e8c2d7b1d)

