# Long term mounting of `/cba/exchange` on Linux 

Author: Emil Melnikov

## Protocol

1. Run `nano ~/.smbcredentials` to create the credentials file with the following contents (replace USERNAME and PASSWORD with your actual username and password):

```
username=USERNAME
password=PASSWORD
domain=EMBL
```

Press Ctrl-O ENTER to save the file, and then Ctrl-X to exit.

2. IMPORTANT: Run `chmod 600 ~/.smbcredentials` to secure the file.

3. Create an entry in the system mount file: 

```
printf "//cba.embl.de/cba/exchange\t$HOME/localmountpoint\tcifs\tuid=$(id -u),gid=$(id -g),file_mode=0644,dir_mode=0755,users,credentials=$HOME/.smbcredentials\t0\t0\n" | sudo tee -a /etc/fstab
```

4. Finally, run `mount ~/localmountpoint` to mount the share, or just go to your file manager and find the groupshare in the sidebar.