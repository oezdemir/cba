## Docker

- [best practices creating a dockerfile](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
- [one or multiple RUN](https://stackoverflow.com/questions/39223249/multiple-run-vs-single-chained-run-in-dockerfile-which-is-better)

### Dockerfile tipps and tricks

- Use `python -m pip install` and not just `pip install` (https://snarky.ca/why-you-should-use-python-m-pip/).
- Probably better to use `RUN conda run --name package_name do_stuff` instead of `RUN conda active package_name && do_stuff`
  - JK: "Actually conda activate may not work by itself because it needs a shell to do some of its work like conda init so you probably need conda init bash somewhere before conda activate."


### How to locally build and modify a Dockerfile

```
git clone git@git.embl.de:grp-jupyterhub/jupyterhub-images.git
rm -rf ~/.docker/config.json ## otherwise the next command may not work...
docker login git.embl.de:4567 # to download an image from an repository
cd jupyterhub-images/02-napari-gpu-desktop
docker build .
vi Dockerfile # change stuff
docker build . # rebuild
```

### Troubleshooting

#### docker login

Error:

```
[+] Building 0.2s (3/3) FINISHED                                                
 => [internal] load build definition from Dockerfile                       0.0s
 => => transferring dockerfile: 731B                                       0.0s
 => [internal] load .dockerignore                                          0.0s
 => => transferring context: 2B                                            0.0s
 => ERROR [internal] load metadata for git.embl.de:4567/grp-jupyterhub/ju  0.1s
------
 > [internal] load metadata for git.embl.de:4567/grp-jupyterhub/jupyterhub-images/jupyter-desktop-server-1.3-noldap:latest:
------
failed to solve with frontend dockerfile.v0: failed to create LLB definition: rpc error: code = Unknown desc = error getting credentials - err: exit status 1, out: `error getting credentials - err: exit status 1, out: `The user name or passphrase you entered is not correct.``
```

Solution: `docker login git.embl.de:4567`

Doing `docker login` you may encounter this error:

```
Error saving credentials: error storing credentials - err: exit status 1, out: `error getting credentials - err: exit status 1, out: `The user name or passphrase you entered is not correct.``
```

Potential solution for Mac: https://github.com/aws/aws-cli/issues/3264

#### docker daemon

Error:

```
Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
```

Solution: You need to ensure that docker is running. On a Mac this is achieved by starting the `Docker.App`

