# Image analysis consultancy

Dear Scientist,

Thank you very much for contacting us!

### Arrange a meeting

We currently offer virtual consultancy twice a week.

- Tuesday 11:00-12:00 CET; [Tuesday Zoom](https://embl-org.zoom.us/j/94755122115?pwd=ZCtPaVp1b2oydm9OUUZaRVdNNWs1UT09)

- Friday 09:30-10:30 CET; [Friday Zoom](https://embl-org.zoom.us/j/94272491667?pwd=cnhwamdwc2hLVVhTcjdDYnltYkFyQT09)

Please simply book one of those slots in [PPMS](https://ppms.embl.de/planning/?item=273). 

Please also let us (image-analysis-support@embl.de) know if that does not work for you or you need help faster and we'll find a different arrangement!


### Prepare for the meeting

#### Important

It would be important to have **two example images** available; ideally already **opened in Fiji** such that we can inspect the raw data in detail together during the meeting; ideally the two images covering the range of phenomena you are investigating (e.g. **positive and negative control**).

#### Optional

If you can, it would be nice if you could prepare a short presentation, roughly following below suggestions. Of course, please modify this according to your project and leave out things that are not applicable. Please don't get stressed about preparing anything, we will help you in any case :) 

- Slide 1: Scientific background
- Slide 2: Sample preparation and image acquisition
  - What's the specimen?
  - What kind of staining did you do?
  - Which microscope did you use? 
- Slide 3: Example images
  - Ideally two images: treated and untreated with a visible difference of what should be measured
  - If above does not apply to your project it would still be nice if you could show more than one image such that we can get a feel for how variable the data is.
- Slide 4: Technicalities
  - Do you already know which number(s) you would like to measure?
  - How many images do you want to analyse (10,100,1000,10000,100000,...)?
  - How big are your individual images (MB, TB)?
  - Would you prefer a certain analysis software?
  - Do you already have experience with a certain analysis software?
  - Did you already try to analyse the data?


Best wishes and we looking forward to meeting you!

image-analysis-support@embl.de




